package hr.fer.rassus.lab.Controllers;

import hr.fer.rassus.lab.UIModels.FilmModel;
import hr.fer.rassus.lab.UIModels.PaymentModel;
import hr.fer.rassus.lab.UIModels.PaymentQuery;
import hr.fer.rassus.lab.UIModels.Query;
import hr.fer.rassus.lab.ServicesImpl.FilmServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
public class FilmController {

    @Autowired
    FilmServiceImpl filmService;

    @PostMapping("/api/films")
    public ResponseEntity<Object> findAllFilms(@RequestBody Query query) {
        if (query == null) {
            return ResponseEntity.badRequest().body("Query is missing.");
        }

        List<FilmModel> foundFilms = filmService.findAllFilms(query);

        return ResponseEntity.ok(foundFilms);
    }

    @GetMapping("/api/films")
    public ResponseEntity<Object> findAllFilmsByTitle(@RequestParam(value = "title") String title) {
        if (title == null) {
            return ResponseEntity.badRequest().body("Title is missing");
        }
        List<FilmModel> foundFilms = filmService.findAllFilmsByTitle(title);

        return ResponseEntity.ok(foundFilms);
    }

    @PostMapping("/api/films/{filmID}/analytics")
    public ResponseEntity<Object> findPayments(@RequestBody PaymentQuery paymentQuery, @PathVariable int filmID) throws ParseException {
        List<PaymentModel> payments = filmService.findAllPayments(paymentQuery, filmID);
        if (payments.size() > 0) {
            return ResponseEntity.ok().body(payments);
        }
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/api/films/{filmID}")
    public ResponseEntity deleteFilm(@PathVariable int filmID) {
        filmService.deleteFilm(filmID);
        return ResponseEntity.ok("Film sa zadanim IDem je uspješno pobrisan");
    }
}
