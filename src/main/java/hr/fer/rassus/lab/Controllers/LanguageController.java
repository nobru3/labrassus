package hr.fer.rassus.lab.Controllers;

import hr.fer.rassus.lab.DBModels.Language;
import hr.fer.rassus.lab.ServicesImpl.LanguageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LanguageController {

    @Autowired
    LanguageServiceImpl languageService;

    @GetMapping("/api/languages")
    public List<Language> findAllLanguages() {
        return languageService.findAllLanguages();
    }
}
