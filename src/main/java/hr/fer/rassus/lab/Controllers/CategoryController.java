package hr.fer.rassus.lab.Controllers;

import hr.fer.rassus.lab.DBModels.Category;
import hr.fer.rassus.lab.ServicesImpl.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CategoryController {

    @Autowired
    private CategoryServiceImpl categoryService;

    @GetMapping("/api/categories")
    public List<Category> findAllCategories() {
        return categoryService.findAllCategories();
    }
}
