package hr.fer.rassus.lab.Specifications;

import hr.fer.rassus.lab.DBModels.Film;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public class FilmSpecifications {

    public static Specification<Film> hasTitle(String title) {
        return (film, cq, cb) -> cb.like(cb.lower(film.get("title")), "%" + title.toLowerCase() + "%");
    }

    public static Specification<Film> isReleaseYearBetween(int minReleaseYear, int maxReleaseYear) {
        return (film, cq, cb) -> cb.between(film.get("releaseYear"), minReleaseYear, maxReleaseYear);
    }

    public static Specification<Film> isLanguageIn(List<Integer> languages) {
        return (film, cq, cb) -> cb.in(film.get("language_id")).value(languages);
    }

    public static Specification<Film> withCategory(List<Integer> categoryIdes) {
        return (film, cq, cb) -> cb.in(film.get("filmCategory").get("category_id")).value(categoryIdes);
    }
}
