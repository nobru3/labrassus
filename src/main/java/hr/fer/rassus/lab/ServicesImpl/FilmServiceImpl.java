package hr.fer.rassus.lab.ServicesImpl;

import hr.fer.rassus.lab.DBModels.Film;
import hr.fer.rassus.lab.UIModels.FilmModel;
import hr.fer.rassus.lab.UIModels.PaymentModel;
import hr.fer.rassus.lab.UIModels.PaymentQuery;
import hr.fer.rassus.lab.UIModels.Query;
import hr.fer.rassus.lab.Repositories.FilmRepository;
import hr.fer.rassus.lab.Services.FilmService;
import hr.fer.rassus.lab.Specifications.FilmSpecifications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.data.jpa.domain.Specification.where;

@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    FilmRepository filmRepository;

    @Override
    @Cacheable(value = "films", key = "#query")
    public List<FilmModel> findAllFilms(Query query) {
        List<Film> foundFilms = filmRepository.
                findAll(where(FilmSpecifications.hasTitle(query.getTitle()))
                        .and(FilmSpecifications.withCategory(query.getCategory()))
                        .and(FilmSpecifications.isReleaseYearBetween(query.getMin_release_year(), query.getMax_release_year()))
                        .and(FilmSpecifications.isLanguageIn(query.getLanguage())));
        return convertToFilmModel(foundFilms);
    }

    @Override
    @Cacheable(value = "payments", key = "T(hr.fer.rassus.lab.ServicesImpl.FilmServiceImpl).getCacheKey(#paymentQuery, #filmID)")
    public List<PaymentModel> findAllPayments(PaymentQuery paymentQuery, int filmID) throws ParseException {
        Date startDate = Date.from(Instant.from(new SimpleDateFormat("yyyy-MM-dd")
                .parse(paymentQuery.getStart_year() + "-" + paymentQuery.getStart_month() + "-01").toInstant()));
        Date endDate = Date.from(Instant.from(new SimpleDateFormat("yyyy-MM-dd")
                .parse(paymentQuery.getEnd_year() + "-" + paymentQuery.getEnd_month() + "-01").toInstant()));

        String[][] paymentObjects = filmRepository.findPaymentAmount(filmID, startDate, endDate);
        List<PaymentModel> paymentModels = new ArrayList<>();
        for (String[] paymentObject : paymentObjects) {
            String month = paymentObject[1].split("\\.")[0];
            String year = paymentObject[2].split("\\.")[0];
            paymentModels.add(new PaymentModel(
                    Double.parseDouble(paymentObject[0]), Integer.parseInt(month), Integer.parseInt(year)));
        }

        return paymentModels;
    }

    @Override
    public void deleteFilm(int filmID) {
        filmRepository.deleteById(filmID);
    }

    public static String getCacheKey(PaymentQuery paymentQuery, int filmID) {
        return filmID + "_" + paymentQuery.toString();
    }

    @Override
    public List<FilmModel> findAllFilmsByTitle(String title) {
        List<Film> foundFilms = filmRepository.findAll(where(FilmSpecifications.hasTitle(title)));
        return convertToFilmModel(foundFilms);
    }

    private List<FilmModel> convertToFilmModel(List<Film> foundFilms) {
        List<FilmModel> filmModels = new ArrayList<>();
        if (foundFilms.size() > 0) {
            for (Film foundFilm : foundFilms) {
                filmModels.add(new FilmModel(foundFilm.getFilmId(), foundFilm.getTitle(), foundFilm.getDescription()));
            }
        }
        return filmModels;
    }
}
