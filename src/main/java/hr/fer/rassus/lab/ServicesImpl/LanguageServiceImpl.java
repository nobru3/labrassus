package hr.fer.rassus.lab.ServicesImpl;

import hr.fer.rassus.lab.DBModels.Language;
import hr.fer.rassus.lab.Repositories.LanguageRepository;
import hr.fer.rassus.lab.Services.LanguagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LanguageServiceImpl implements LanguagesService {

    @Autowired
    LanguageRepository languageRepository;

    @Override
    public List<Language> findAllLanguages() {
        return languageRepository.findAll();
    }
}
