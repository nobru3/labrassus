package hr.fer.rassus.lab.ServicesImpl;

import hr.fer.rassus.lab.DBModels.Category;
import hr.fer.rassus.lab.Repositories.CategoryRepository;
import hr.fer.rassus.lab.Services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> findAllCategories() {
        return categoryRepository.findAll();
    }
}
