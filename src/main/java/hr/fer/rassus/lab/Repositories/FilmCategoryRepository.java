package hr.fer.rassus.lab.Repositories;

import hr.fer.rassus.lab.DBModels.FilmCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmCategoryRepository extends JpaRepository<FilmCategory, Integer> {
}
