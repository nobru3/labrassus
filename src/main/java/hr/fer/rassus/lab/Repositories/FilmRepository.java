package hr.fer.rassus.lab.Repositories;

import hr.fer.rassus.lab.DBModels.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface FilmRepository extends JpaRepository<Film, Integer>, JpaSpecificationExecutor<Film> {

    @Query(value = "SELECT SUM(payment.amount) as amount, EXTRACT(MONTH from rental.rental_date) as mjesec, EXTRACT(YEAR from rental.rental_date) as godina from rental inner join inventory using (inventory_id) inner join payment using(rental_id) where inventory.film_id = ?1 and rental.rental_date > ?2 and rental.rental_date < ?3 group by EXTRACT(MONTH from rental.rental_date), EXTRACT(YEAR from rental.rental_date) order by godina,mjesec", nativeQuery = true)
    String[][] findPaymentAmount(int filmId, Date startDate, Date endDate);

}
