package hr.fer.rassus.lab.Repositories;

import hr.fer.rassus.lab.DBModels.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LanguageRepository extends JpaRepository<Language, Integer> {
}
