package hr.fer.rassus.lab.Services;

import hr.fer.rassus.lab.UIModels.FilmModel;
import hr.fer.rassus.lab.UIModels.PaymentModel;
import hr.fer.rassus.lab.UIModels.PaymentQuery;
import hr.fer.rassus.lab.UIModels.Query;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public interface FilmService {

    List<FilmModel> findAllFilms(Query query);

    List<PaymentModel> findAllPayments(PaymentQuery paymentQuery, int filmID) throws ParseException;

    void deleteFilm(int filmID);

    List<FilmModel> findAllFilmsByTitle(String title);

}
