package hr.fer.rassus.lab.Services;

import hr.fer.rassus.lab.DBModels.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAllCategories();
}
