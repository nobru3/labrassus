package hr.fer.rassus.lab.Services;

import hr.fer.rassus.lab.DBModels.Language;

import java.util.List;

public interface LanguagesService {
    List<Language> findAllLanguages();
}
