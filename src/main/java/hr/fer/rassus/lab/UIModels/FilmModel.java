package hr.fer.rassus.lab.UIModels;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FilmModel implements Serializable {

    private int film_id;
    private String title;
    private String description;
}
