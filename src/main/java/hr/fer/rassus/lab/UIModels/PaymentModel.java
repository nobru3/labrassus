package hr.fer.rassus.lab.UIModels;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PaymentModel implements Serializable {

    private Double amount;
    private Integer month;
    private Integer year;
}
