package hr.fer.rassus.lab.UIModels;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Query {

    private String title;
    private int min_release_year;
    private int max_release_year;
    private List<Integer> language;
    private List<Integer> category;

}
