package hr.fer.rassus.lab.UIModels;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PaymentQuery {

    private int start_month;
    private int start_year;
    private int end_month;
    private int end_year;
}
