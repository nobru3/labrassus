package hr.fer.rassus.lab.DBModels;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Film implements Serializable {

    @Id
    private int filmId;
    private String title;
    private String description;
    private int releaseYear;
    private int language_id;
    private int rentalDuration;
    private int rentalRate;
    private int length;
    private double replacementCost;
    private LocalDateTime lastUpdateFilm;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "film_id", referencedColumnName = "film_id")
    private FilmCategory filmCategory;
}


