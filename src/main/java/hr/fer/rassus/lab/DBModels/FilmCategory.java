package hr.fer.rassus.lab.DBModels;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FilmCategory implements Serializable {

    @Id
    private int film_id;
    private int category_id;
    private LocalDateTime lastUpdateFilmCategory;
}
